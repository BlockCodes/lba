pragma solidity ^0.6.4;

contract LBAChain {
  enum LandType{
    noLand,
    coastal_belt,
    water_logged_land,
    rocky_land,
    waste_land,
    wet_land,
    gov_property}
  enum UserType{noUser,user,officer}

  struct LandStruct{
    LandType landType;
    uint area;
    bool isActive;
  }
  struct UserStruct{
    string name;
    uint aadharId;
    uint phoneNo;
    uint wardNo;
    UserType userType;
  }
  struct OfficerStruct{
    string name;
    uint aadharId;
    uint phoneNo;
    uint villageNo;
    uint[] wardList;
    UserType userType;
  }
  struct LandRegisterStruct{
    uint wardNo;
    uint surveyNo;
    uint ownerId;
    uint officerId;
  }
  struct ChangeLandPropertyStruct{
    uint wardNo;
    uint surveyNo;
    uint ownerId;
    LandType currentLandType;
    LandType changedLandType;
    uint[] approverdUsers;
    uint approverdOfficer;
    uint approvedTime;
    bool valid;
  }
  struct TaxPaymentStruct{
    uint wardNo;
    uint surveyNo;
    LandType landType;
    uint area;
    uint amount;
  }
  // userId => UserStruct
  mapping (uint => UserStruct) public user;
  // tax_id => TaxPaymentStruct
  mapping (uint => TaxPaymentStruct) public taxData;

  // userId => OfficerStruct
  mapping (uint => OfficerStruct) public officer;

  // userId  = surveyNoList
  mapping (uint => uint[] )  landIdList;

  // userAddress => userId
  mapping (address=>uint) public userId;

  // orderId => LandRegisterStruct
  mapping (uint=>LandRegisterStruct) public landAllotment;

  // surveyNo => LandStruct
  mapping (uint=>LandStruct) public land;
  // surveyNo => userId
  mapping (uint=>uint) public landOwner;
  // wardNo => clp_id
  mapping (uint=>uint[]) clpList;
  // changeLandPropertyId => ChangeLandPropertyStruct
  mapping (uint=>ChangeLandPropertyStruct) public changeLand;

  // nested mapping for approver to check whether he is voted already
  mapping (address => mapping (uint => bool)) public isApproved;

  //userId
  uint public u_ID;

  //surveyId
  uint public s_ID;

  //orderId
  uint public o_ID;
  //tax_id
  uint public t_id;

  //changeLandPropertyId
  uint public clp_ID;
  uint public tax_price = 0.00005 ether;
  address payable admin;

  modifier onlyAdmin() {
    require(msg.sender == admin, "Only Admin Can Do that");
    _;
  }
  modifier onlyOfficer() {
    require(uint(officer[userId[msg.sender]].userType) == 2, "Only Officer Can Do that");
    _;
  }
  modifier mustNewUser() {
    require(userId[msg.sender] == 0, "Only New User Can Do that");
    _;
  }
  modifier mustWardControl(address _userAddress) {
    require(userId[_userAddress] != 0, "User Must be Registerd");
      uint[] memory a = officer[userId[msg.sender]].wardList;
    require( indexOf(a,user[userId[_userAddress]].wardNo) != uint(-1), "User and Officer must be in Same ward");
    _;
  }

  modifier OnlyOwnerTransferLand(uint _s_ID,uint _area) {
     uint[] memory a = landIdList[userId[msg.sender]];
     require( landOwner[_s_ID] == userId[msg.sender], "User must have the land ");
     require( land[_s_ID].area >= _area, " Given area is low ");
     require( land[_s_ID].isActive == true, " Land Is under observation ");
     _;
   }

   modifier OnlyOwnerChangeLandProperty(uint _s_ID) {
     require( landOwner[_s_ID] == userId[msg.sender], "User must have the land ");
     _;
   }

  modifier VotingEligibility(uint _clp_ID) {
    require(userId[msg.sender] != 0, "User Must be Registerd");
    UserStruct memory tuser =  user[userId[msg.sender]];
    require(changeLand[_clp_ID].ownerId != userId[msg.sender], "Owner can't Voted");
    require(isApproved[msg.sender][_clp_ID] == false, "User Already Voted");
    require(tuser.wardNo == changeLand[_clp_ID].wardNo, "User Must be in Same Ward");
    // require(changeLand[_clp_ID].approverdUsers.length < 2,"Only 2 votes currently allowed");
    _;
  }
  modifier ApproveEligibility(uint _clp_ID) {
    require(userId[msg.sender] != 0, "Officer Must be Registerd");
    OfficerStruct memory tofficer =  officer[userId[msg.sender]];
    require(changeLand[_clp_ID].ownerId != userId[msg.sender], "Owner can't Voted");
    require(isApproved[msg.sender][_clp_ID] == false, "Officer Already Voted");
    uint[] memory a = officer[userId[msg.sender]].wardList;
    require( indexOf(a,changeLand[_clp_ID].wardNo) != uint(-1), "Officer Must be in Same Ward");
    require(changeLand[_clp_ID].approverdUsers.length >= 2,"Need 2 User votes From Same Ward");
    _;
  }
  event Registraion(address indexed user, string name);
  event LandRegistraion(uint surveyNo);
  event TaxPayment(uint transcationId);
  event LandTranscation(uint surveyNo);
  event ChangeLand(uint changeLandId);

  constructor () public {
    admin = msg.sender;
  }
  function UserRegistraion(
    string calldata _name,
    uint _aadharId,
    uint _wardNo,
    uint _phoneNo
  ) external mustNewUser {
    u_ID++;
    userId[msg.sender] = u_ID;
    user[u_ID] = UserStruct(_name,_aadharId,_phoneNo,_wardNo ,UserType(1));
    emit Registraion(msg.sender, _name);
  }
  function OfficerRegistraion(
    string calldata _name,
    uint _aadharId,
    uint _phoneNo,
    uint _villageNo,
    uint[] calldata _wardlist,
    address _userAddress
  ) external onlyAdmin {
      u_ID++;
      userId[_userAddress] = u_ID;
      officer[u_ID] = OfficerStruct(_name,_aadharId,_phoneNo,_villageNo,_wardlist,UserType(2));
      emit Registraion(_userAddress, _name);
  }

  function NewLandRegistraion(
    address _userAddress,
    LandType _landType,
    uint _area) external onlyOfficer mustWardControl(_userAddress)  {
      require(uint(_landType)>0,'land Must not be noLand');
      s_ID++;
      land[s_ID] = LandStruct(_landType,_area,true);
      o_ID++;
      UserStruct memory tUser = user[userId[_userAddress]];
      landAllotment[o_ID] = LandRegisterStruct(tUser.wardNo,s_ID,userId[_userAddress],userId[msg.sender]);
      landIdList[userId[_userAddress]].push(s_ID);
      landOwner[s_ID] = userId[_userAddress];
      emit LandRegistraion(s_ID);
  }

  function PayTax(uint _s_ID) external payable {
    t_id++;
    taxData[t_id]=TaxPaymentStruct(
      user[userId[msg.sender]].wardNo,
      _s_ID,
      land[_s_ID].landType,
      land[_s_ID].area,
      land[_s_ID].area*tax_price
      );
      admin.transfer(msg.value);
      emit TaxPayment(t_id);
  }
  function TransferLand(
    uint _s_ID,
    uint _area,
    address _buyer) external OnlyOwnerTransferLand(_s_ID,_area) {
      land[_s_ID].area-=_area;
      if (land[_s_ID].area == 0) {
        uint index = indexOf(landIdList[userId[msg.sender]],_s_ID);
        delete landIdList[userId[msg.sender]][index];
        delete landOwner[_s_ID];
      }
      s_ID++;
      land[s_ID] = LandStruct(land[_s_ID].landType,_area,true);
      landIdList[userId[_buyer]].push(s_ID);
      landOwner[s_ID] = userId[_buyer];
      emit LandTranscation(s_ID);
  }

  function ChangeLandProperty(
    uint _s_ID,
    LandType _newlandType) OnlyOwnerChangeLandProperty(_s_ID) external {
      uint[] memory a = new uint[](0);
      UserStruct memory tuser = user[userId[msg.sender]];
      clp_ID++;
      changeLand[clp_ID] = ChangeLandPropertyStruct(
        tuser.wardNo,
        _s_ID,
        userId[msg.sender],
        land[_s_ID].landType,
        _newlandType,a,0,0,false
      );
      land[_s_ID].isActive = false;
      clpList[tuser.wardNo].push(clp_ID);
      emit ChangeLand(clp_ID);
  }

  function Voting(uint _clp_ID) external VotingEligibility(_clp_ID) {
    changeLand[_clp_ID].approverdUsers.push(userId[msg.sender]);
    isApproved[msg.sender][_clp_ID] = true;
  }

  function Approve(uint _clp_ID) external ApproveEligibility(_clp_ID) {
    changeLand[_clp_ID].approverdOfficer = userId[msg.sender];
    changeLand[_clp_ID].approvedTime = now;
    isApproved[msg.sender][_clp_ID] = true;
    land[changeLand[_clp_ID].surveyNo] = LandStruct(changeLand[_clp_ID].changedLandType,land[changeLand[_clp_ID].surveyNo].area,true);
    uint clpIndex = indexOf(clpList[changeLand[_clp_ID].wardNo],_clp_ID);
    delete clpList[changeLand[_clp_ID].wardNo][clpIndex];
  }
  function indexOf(uint[] memory self, uint value) internal pure returns (uint) {
    for (uint i = 0; i < self.length; i++)
      if (self[i] == value) return i;
    return uint(-1);
  }

  // Read Functions

  function LandList(uint _userId) external view returns(uint[] memory){
    return(landIdList[_userId]);
  }
  function ApproverdUsers(uint _clp_ID) external view returns(uint [] memory){
    return(changeLand[_clp_ID].approverdUsers);
  }
  function OfficerWardList(uint _officerId) external view returns(uint [] memory){
    return(officer[_officerId].wardList);
  }
  function IsApproved(uint _clp_ID) external view returns(bool){
    return(isApproved[msg.sender][_clp_ID]);
  }
  function CheckUser() external view returns(uint){
    if (msg.sender == admin) {
      return 3;
    }
    if (uint(officer[userId[msg.sender]].userType) == 2) {
      return 2;
    }
    if (uint(user[userId[msg.sender]].userType) == 1) {
      return 1;
    }
    return 0;
  }
  function WardVoteList(uint _wardNo) external view returns(uint[] memory) {
    return (clpList[_wardNo]);
  }
}
