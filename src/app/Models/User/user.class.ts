import { UserModel } from './user.model'

export class UserClass implements UserModel {
  name: string
  aadharId: number
  phoneNo: number
  wardNo: number
}
