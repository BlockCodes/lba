export interface UserModel {
  name: string
  aadharId: number
  phoneNo: number
  wardNo: number
}
