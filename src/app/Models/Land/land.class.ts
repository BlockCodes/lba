import { OwnedLandModel } from './land.model'

export class LandClass implements OwnedLandModel {
  index: number
  survayNo: any
  area: any
  landType: any
  landName: any
}
