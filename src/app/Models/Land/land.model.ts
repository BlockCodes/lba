export interface OwnedLandModel {
  index: number
  survayNo: any
  area: any
  landType: any
  landName: any
  isActive: boolean
}
export interface ChangeLandModel {
  index: number
  cl_id: number
  survayNo: any
  currentLandType: any
  propossedlandType: any
  totelVotes: any
}
export interface ApproveLandModel {
  index: number
  cl_id: number
  wardNo: number
  survayNo: any
  currentLandType: any
  propossedlandType: any
  totelVotes: any
}
export interface LandModel {
  type: string
  value: any
}
export interface RegisterLandModel {
  address: string
  area: any
  type: any
}
