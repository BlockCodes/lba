import { OfficerModel } from './officer.model'

export class OfficerClass implements OfficerModel {
  name: string
  aadharId: number
  phoneNo: number
  villageNo: number
  wardList: any
  address: string
}
