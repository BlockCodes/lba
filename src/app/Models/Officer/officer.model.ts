export interface OfficerModel {
  name: string
  aadharId: number
  phoneNo: number
  villageNo: number
  wardList: any
  address: string
}
