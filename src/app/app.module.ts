import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { HomeComponent } from './Components/home/home.component'
import { UserRegComponent } from './Components/user-reg/user-reg.component'
import { AdminComponent } from './Components/admin/admin.component'
import { UserComponent } from './Components/user-router/user/user.component'
import { NavigationComponent } from './Components/navigation/navigation.component'
import { MaterialModule } from './Modules/material/material.module'
import { FaqComponent } from './Components/faq/faq.component'
import { AboutComponent } from './Components/about/about.component'
import { FormsModule } from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { OfficerRouterComponent } from './Components/officer-router/officer-router.component'
import { OfficerComponent } from './Components/officer-router/officer/officer.component'
import { LandRegComponent } from './Components/officer-router/land-reg/land-reg.component'
import { ApproveComponent } from './Components/officer-router/approve/approve.component'
import { UserRouterComponent } from './Components/user-router/user-router.component'
import { UserApproveComponent } from './Components/user-router/user-approve/user-approve.component'
import { ModalModule } from './Modules/_modal';
import { ViewDetailsComponent } from './Components/view-details/view-details.component'

@NgModule( {
  declarations: [
    AppComponent,
    HomeComponent,
    UserRegComponent,
    AdminComponent,
    OfficerComponent,
    UserComponent,
    NavigationComponent,
    FaqComponent,
    AboutComponent,
    LandRegComponent,
    ApproveComponent,
    OfficerRouterComponent,
    UserRouterComponent,
    UserApproveComponent,
    ViewDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MaterialModule, ModalModule
  ],
  providers: [],
  entryComponents: [

  ViewDetailsComponent],
  bootstrap: [ AppComponent ]
} )
export class AppModule { }
