import { Component, OnInit } from '@angular/core';
import { Web3Service } from 'src/app/Services/Web3/web3.service';
import { BreakpointObserver } from '@angular/cdk/layout'
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private web3service: Web3Service,
    private breakpointObserver: BreakpointObserver,
    private route: Router
    ) { }

  ngOnInit() {
  }
  logInUser = async () => {
    this.web3service.web3login()
    this.route.navigateByUrl('/user')
  }
  logInUserReg = async () => {
    this.web3service.web3login()
    this.route.navigateByUrl('/user-reg')
  }
  logInOfficer = async () => {
    this.web3service.web3login()
    this.route.navigateByUrl('/officer')
  }
  logInAdmin = async () => {
    this.web3service.web3login()
    this.route.navigateByUrl('/admin')
  }
}
