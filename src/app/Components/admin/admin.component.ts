import { Component, OnInit } from '@angular/core'
import { NgForm } from '@angular/forms'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3.model'
import { OfficerModel } from 'src/app/Models/Officer/officer.model'
@Component( {
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: [ './admin.component.scss' ]
} )
export class AdminComponent implements OnInit {
  account: string
  lba: any
  constructor ( private web3service: Web3Service, private route: Router ) { }
  ngOnInit() {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
  }
  onSubmit = async ( form: NgForm ) => {
    try {
      if ( form.valid ) {
        const officer: OfficerModel = form.value
        officer.wardList = officer.wardList.split( ',' ).map( x => +x )
        console.log( 'Log: AdminComponent -> onSubmit -> officer', officer )
        const regRespone = await this.lba.OfficerRegistraion(
          officer.name,
          officer.aadharId,
          officer.phoneNo,
          officer.villageNo,
          officer.wardList,
          officer.address
        ).send( { from: this.account, gas: 5000000 } )
        console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
        if ( regRespone.status ) {
          alert( regRespone.events.Registraion.returnValues.name + '  is Registred Successfully' )
          form.resetForm()
        }
      }
    } catch ( error ) {
      console.log( 'Log: RegisterComponent -> onSubmit -> error', error )

    }
  }

}
