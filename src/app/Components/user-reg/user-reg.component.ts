import { Component, OnInit } from '@angular/core'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3.model'
import { NgForm } from '@angular/forms'
import { UserModel } from 'src/app/Models/User/user.model'

@Component( {
  selector: 'app-user-reg',
  templateUrl: './user-reg.component.html',
  styleUrls: [ './user-reg.component.scss' ]
} )
export class UserRegComponent implements OnInit {
  account: string
  lba: any
  constructor ( private web3service: Web3Service, private route: Router ) { }
  ngOnInit() {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
  }
  onSubmit = async ( form: NgForm ) => {
    try {
      if ( form.valid ) {
        const user: UserModel = form.value
        console.log( 'Log: AdminComponent -> onSubmit -> user', user )
        const regRespone = await this.lba.UserRegistraion(
          user.name,
          user.aadharId,
          user.wardNo,
          user.phoneNo,
        ).send( { from: this.account, gas: 5000000 } )
        console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
        if ( regRespone.status ) {
          alert( 'Hi ' + regRespone.events.Registraion.returnValues.name + ' You are Registred Successfully' )
          form.resetForm()
        }
      }
    } catch ( error ) {
      console.log( 'Log: RegisterComponent -> onSubmit -> error', error )

    }
  }

}
