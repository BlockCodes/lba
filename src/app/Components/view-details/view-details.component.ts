import { Component, OnInit } from "@angular/core"
import { Web3Service } from "src/app/Services/Web3/web3.service"
import { Router } from "@angular/router"
import { ModalService } from "src/app/Modules/_modal"
import { Web3Model } from "src/app/Models/web3.model"
import { LandModel } from 'src/app/Models/Land/land.model'
declare let web3: any
export interface TransactionModel {
  wardNo: any
  surveyNo: any
  landName: any
  area: any
  amount: any
}
const Land: LandModel[] = [
  { type: 'Coastal Belt', value: 1 },
  { type: 'Water Logged Land', value: 2 },
  { type: 'Rocky Land', value: 3 },
  { type: 'Waste Land', value: 4 },
  { type: 'Wet Land', value: 5 },
  { type: 'Gov Property', value: 6 }
]
@Component( {
  selector: "app-view-details",
  templateUrl: "./view-details.component.html",
  styleUrls: [ "./view-details.component.scss" ],
} )
export class ViewDetailsComponent implements OnInit {
  account: any
  lba: any
  transId: any
  surveyNo: any
  changelandId: any
  data: any
  Land = Land

  transcationModal: boolean
  surveyModal: boolean
  changelandIdModal: boolean

  constructor (
    private web3service: Web3Service,
    private route: Router,
    private modalService: ModalService
  ) { }

  ngOnInit() {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
  }
  closeModal( id: string ) {
    this.modalService.close( id )
    this.transcationModal = false
    this.surveyModal = false
    this.changelandIdModal = false
    this.transId = undefined
    this.surveyNo = undefined
    this.changelandId = undefined
  }
  changeLandSearch = async ( id: string ) => {
    try {
      const details = await this.lba.changeLand( parseInt( this.changelandId ) ).call( { from: this.account } )
      const approverdUsers = await this.lba.ApproverdUsers( parseInt( this.changelandId ) ).call( { from: this.account } )
      let tName = await this.Land.find( o => o.value == details.currentLandType )
      let pName = await this.Land.find( o => o.value == details.changedLandType )
      const t = {
        wardNo: details.wardNo,
        surveyNo: details.surveyNo,
        ctype: tName.type,
        ptype: pName.type,
        users: approverdUsers,
        Officer: details.approverdOfficer,
      }
      console.log( 'Log: ViewDetailsComponent -> changeLandSearch -> t', t )
      this.data = t
      this.modalService.open( id )
      this.changelandIdModal = true
    } catch ( error ) {
      alert( 'Invalid Changeland Id' )

      this.changelandId = undefined
      console.log( 'Log: ViewDetailsComponent -> changeLandSearch -> error', error )
    }
  };
  transIdSearch = async ( id: string ) => {
    try {
      const details = await this.lba.taxData( parseInt( this.transId ) ).call( { from: this.account } )
      let tName = await this.Land.find( o => o.value == details.landType )
      const t: TransactionModel = {
        amount: web3.utils.fromWei( String( details.amount ), 'ether' ),
        wardNo: details.wardNo,
        landName: tName.type,
        area: details.area,
        surveyNo: details.surveyNo
      }
      console.log( 'Log: ViewDetailsComponent -> transIdSearch -> t', t )
      this.data = t
      this.modalService.open( id )
      this.transcationModal = true
    } catch ( error ) {
      alert( 'Invalid Transcation Id' )
      this.transId = undefined

      console.log( 'Log: ViewDetailsComponent -> transIdSearch -> error', error )
    }
  };

  surveySearch = async ( id: string ) => {
    try {
      const details = await this.lba.land( parseInt( this.surveyNo ) ).call( { from: this.account } )
      let tName = await this.Land.find( o => o.value == details.landType )
      const t = {
        area: details.area,
        isActive: details.isActive,
        landName: tName.type,
      }
      console.log( 'Log: ViewDetailsComponent -> surveySearch -> t', t )
      this.data = t
      this.modalService.open( id )
      this.surveyModal = true
    } catch ( error ) {
      alert( 'Invalid Survey Id' )
      this.surveyNo = undefined
      console.log( 'Log: ViewDetailsComponent -> surveySearch -> error', error )
    }
  };
}
