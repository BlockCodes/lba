import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficerRouterComponent } from './officer-router.component';

describe('OfficerRouterComponent', () => {
  let component: OfficerRouterComponent;
  let fixture: ComponentFixture<OfficerRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficerRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficerRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
