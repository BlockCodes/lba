import { Component, OnInit } from '@angular/core'
import { LandModel, ChangeLandModel } from 'src/app/Models/Land/land.model'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3.model'
const Land: LandModel[] = [
  { type: 'Coastal Belt', value: 1 },
  { type: 'Water Logged Land', value: 2 },
  { type: 'Rocky Land', value: 3 },
  { type: 'Waste Land', value: 4 },
  { type: 'Wet Land', value: 5 },
  { type: 'Gov Property', value: 6 }
]

@Component( {
  selector: 'app-approve',
  templateUrl: './approve.component.html',
  styleUrls: [ './approve.component.scss' ]
} )
export class ApproveComponent implements OnInit {
  dataSource = null
  account: string
  lba: any
  Land = Land
  landList: string[] = [];
  wardNos: any
  userId: any
  changeLandList: ChangeLandModel[] = []
  constructor ( private web3service: Web3Service, private route: Router ) {
    this.landList = JSON.parse( localStorage.getItem( 'landList' ) )
    this.wardNos = JSON.parse( localStorage.getItem( 'wardNos' ) )
    this.userId = JSON.parse( localStorage.getItem( 'userId' ) )
  }

  ngOnInit() {
    if ( this.wardNos === null ) {
      this.route.navigateByUrl( '/officer' )
    }
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
    this.onLoad()
  }
  onLoad = async () => {
    try {
      const s = this.landList
      let WardVoteList = []
      for ( let i = 0;i < this.wardNos.length;i++ ) {
        const WardVote = await this.lba.WardVoteList( parseInt( this.wardNos[ i ] ) ).call( { from: this.account } )
        WardVoteList = WardVoteList.concat( WardVote )
      }
      for ( let index = 0;index < WardVoteList.length;index++ ) {
        if ( WardVoteList[ index ] !== '0' ) {
          const ApproverdUsers = await this.lba.ApproverdUsers( parseInt( WardVoteList[ index ] ) ).call( { from: this.account } )
          const status = await this.lba.IsApproved( WardVoteList[ index ] ).call( { from: this.account } )
          if ( !status ) {
            const changeLand = await this.lba.changeLand( parseInt( WardVoteList[ index ] ) ).call( { from: this.account } )
            let temp: ChangeLandModel
            let clName = await this.Land.find( o => o.value == changeLand.currentLandType )
            let plName = await this.Land.find( o => o.value == changeLand.changedLandType )
            temp = {
              index: index + 1,
              cl_id: WardVoteList[ index ],
              survayNo: changeLand.surveyNo,
              currentLandType: clName.type,
              propossedlandType: plName.type,
              totelVotes: ApproverdUsers.length,
            }
            this.changeLandList.push( temp )
          }
        }
        this.dataSource = this.changeLandList
      }
    } catch ( error ) {
      console.log( 'Log: UserComponent -> onLoad -> error', error )

    }

  }
  vote = async ( cl_id ) => {
    const regRespone = await this.lba.Approve(
      parseInt( cl_id )
    ).send( { from: this.account, gas: 5000000 } )
    console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
    if ( regRespone.status ) {
      this.changeLandList = []
      this.dataSource = null
      this.onLoad()
    }
  }
}
