import { async } from '@angular/core/testing'
import { Component, OnInit } from '@angular/core'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3.model'
import { OfficerClass } from 'src/app/Models/Officer/officer.class'

@Component( {
  selector: 'app-officer',
  templateUrl: './officer.component.html',
  styleUrls: [ './officer.component.scss' ]
} )
export class OfficerComponent implements OnInit {
  account: string
  lba: any
  officerId: any
  officer = new OfficerClass()
  wardList: any
  constructor ( private web3service: Web3Service, private route: Router ) { }
  ngOnInit() {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
    this.onLoad()
  }
  onLoad = async () => {
    this.officerId = await this.lba.userId( this.account ).call( { from: this.account } )
    localStorage.setItem( 'userId', this.officerId )
    this.officer = await this.lba.officer( this.officerId ).call( { from: this.account } )
    this.wardList = await this.lba.OfficerWardList( this.officerId ).call( { from: this.account } )
    localStorage.setItem( 'wardNos', JSON.stringify( this.wardList ) )
  }
}
