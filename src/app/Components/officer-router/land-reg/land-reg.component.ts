
import { Component, OnInit } from '@angular/core'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3.model'
import { NgForm } from '@angular/forms'
import { LandModel, RegisterLandModel } from 'src/app/Models/Land/land.model'
@Component( {
  selector: 'app-land-reg',
  templateUrl: './land-reg.component.html',
  styleUrls: [ './land-reg.component.scss' ]
} )


export class LandRegComponent implements OnInit {
  account: string
  lba: any
  Land: LandModel[] = [
    { type: 'Coastal Belt', value: 1 },
    { type: 'Water Logged Land', value: 2 },
    { type: 'Rocky Land', value: 3 },
    { type: 'Waste Land', value: 4 },
    { type: 'Wet Land', value: 5 },
    { type: 'Gov Property', value: 6 }
  ]
  constructor ( private web3service: Web3Service, private route: Router ) { }
  ngOnInit() {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
  }
  onSubmit = async ( form: NgForm ) => {
    try {
      if ( form.valid ) {
        const land: RegisterLandModel = form.value
        console.log( 'Log: LandRegComponent -> onSubmit -> land', land )
        const regRespone = await this.lba.NewLandRegistraion(
          land.address,
          parseInt( land.type ),
          parseInt( land.area )
        ).send( { from: this.account, gas: 5000000 } )
        console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
        if ( regRespone.status ) {
          alert( 'Land ' + regRespone.events.LandRegistraion.returnValues.surveyNo + '  is Registred Successfully' )
          form.resetForm()
        }
      }
    } catch ( error ) {
      console.log( 'Log: RegisterComponent -> onSubmit -> error', error )
    }
  }
}
