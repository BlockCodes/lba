
import { Component, OnInit } from '@angular/core'
import { LandModel, ChangeLandModel } from 'src/app/Models/Land/land.model'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3.model'
const Land: LandModel[] = [
  { type: 'Coastal Belt', value: 1 },
  { type: 'Water Logged Land', value: 2 },
  { type: 'Rocky Land', value: 3 },
  { type: 'Waste Land', value: 4 },
  { type: 'Wet Land', value: 5 },
  { type: 'Gov Property', value: 6 }
]
@Component( {
  selector: 'app-user-approve',
  templateUrl: './user-approve.component.html',
  styleUrls: [ './user-approve.component.scss' ]
} )
export class UserApproveComponent implements OnInit {
  dataSource = null
  account: string
  lba: any
  Land = Land
  landList: string[] = [];
  wardNo: any
  userId: any
  changeLandList: ChangeLandModel[] = []
  constructor ( private web3service: Web3Service, private route: Router ) {
    this.landList = JSON.parse( localStorage.getItem( 'landList' ) )
    this.wardNo = JSON.parse( localStorage.getItem( 'wardNo' ) )
    this.userId = JSON.parse( localStorage.getItem( 'userId' ) )
  }

  ngOnInit() {
    if ( this.wardNo === null ) {
      this.route.navigateByUrl( '/user' )
    }
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
    this.onLoad()
  }
  onLoad = async () => {
    try {
      const s = this.landList
      console.log( 'Log: UserApproveComponent -> onLoad -> s', s )
      let WardVoteList = await this.lba.WardVoteList( this.wardNo ).call( { from: this.account } )
      console.log( 'Log: UserApproveComponent -> onLoad -> WardVoteList', WardVoteList )
      let t = []
      for ( let index = 0;index < WardVoteList.length;index++ ) {
        const item = WardVoteList[ index ]
        if ( item !== '0' ) {
          const changeLand = await this.lba.changeLand( item ).call( { from: this.account } )
          if ( !s.includes( changeLand.surveyNo ) ) {
            t.push( item )
          }
        }
      }
      for ( let index = 0;index < t.length;index++ ) {
        if ( t[ index ] !== '0' ) {
          const ApproverdUsers = await this.lba.ApproverdUsers( t[ index ] ).call( { from: this.account } )
          if ( !ApproverdUsers.includes( this.userId ) ) {
            const changeLand = await this.lba.changeLand( t[ index ] ).call( { from: this.account } )
            let temp: ChangeLandModel
            let clName = await this.Land.find( o => o.value == changeLand.currentLandType )
            let plName = await this.Land.find( o => o.value == changeLand.changedLandType )
            temp = {
              index: index + 1,
              cl_id: t[ index ],
              survayNo: changeLand.surveyNo,
              currentLandType: clName.type,
              propossedlandType: plName.type,
              totelVotes: ApproverdUsers.length,
            }
            this.changeLandList.push( temp )
          }
        }
        this.dataSource = this.changeLandList
      }
    } catch ( error ) {
      console.log( 'Log: UserComponent -> onLoad -> error', error )

    }

  }
  vote = async ( cl_id ) => {
    
    const regRespone = await this.lba.Voting(
      parseInt( cl_id )
    ).send( { from: this.account, gas: 5000000 } )
    console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
    if ( regRespone.status ) {
      this.changeLandList = []
      this.dataSource = null
      this.onLoad()
    }
  }
}
