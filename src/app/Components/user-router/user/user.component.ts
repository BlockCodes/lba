import { async } from '@angular/core/testing'

import { Component, OnInit, Inject } from '@angular/core'
import { Web3Service } from 'src/app/Services/Web3/web3.service'
import { Router } from '@angular/router'
import { Web3Model } from 'src/app/Models/web3.model'
import { UserClass } from 'src/app/Models/User/user.class'
import { OwnedLandModel, LandModel } from 'src/app/Models/Land/land.model'
import { ModalService } from 'src/app/Modules/_modal'
import { NgForm } from '@angular/forms'

declare let web3: any

const Land: LandModel[] = [
  { type: 'Coastal Belt', value: 1 },
  { type: 'Water Logged Land', value: 2 },
  { type: 'Rocky Land', value: 3 },
  { type: 'Waste Land', value: 4 },
  { type: 'Wet Land', value: 5 },
  { type: 'Gov Property', value: 6 }
]
export interface TransferModel {
  survayNo: any
  area: any
  address: string
}

@Component( {
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: [ './user.component.scss' ]
} )
export class UserComponent implements OnInit {
  displayedColumns: string[] = [
    "index",
    "survayNo",
    "landName",
    "area",
    "transfer",
    "change",
    "pay",
  ];
  dataSource = null
  account: string
  lba: any
  userId: any
  user = new UserClass()
  landList: number[]
  userLandList: OwnedLandModel[] = []
  Land = Land

  survayNo: string
  area: number
  landType: number
  transferModal: boolean
  changeModal: boolean
  constructor ( private web3service: Web3Service, private route: Router, private modalService: ModalService ) {
  }

  ngOnInit() {
    this.web3service.web3login()
    this.web3service.Web3Details$.subscribe( async ( data: Web3Model ) => {
      this.account = data.account
      this.lba = data.lba
    } )
    this.onLoad()
  }
  onLoad = async () => {
    try {
      this.userId = await this.lba.userId( this.account ).call( { from: this.account } )
      localStorage.setItem( 'userId', JSON.stringify( this.userId ) )
      this.user = await this.lba.user( this.userId ).call( { from: this.account } )
      localStorage.setItem( 'wardNo', JSON.stringify( this.user.wardNo ) )
      this.landList = await this.lba.LandList( this.userId ).call( { from: this.account } )
      localStorage.setItem( 'landList', JSON.stringify( this.landList ) )
      this.landList.forEach( async ( sNo, i ) => {
        let temp: OwnedLandModel
        const t: any = await this.lba.land( sNo ).call( { from: this.account } )
        let tName = await this.Land.find( o => o.value == t.landType )
        temp = {
          area: t.area,
          index: i + 1,
          survayNo: sNo,
          landName: tName.type,
          landType: t.landType,
          isActive: t.isActive
        }
        console.log( 'Log: UserComponent -> onLoad -> temp', temp )
        this.userLandList.push( temp )
      } )
      this.dataSource = this.userLandList
      console.log( 'Log: UserComponent -> onLoad -> this.dataSource', this.dataSource )

    } catch ( error ) {
      console.log( 'Log: UserComponent -> onLoad -> error', error )

    }

  }
  transfer = async ( id: string, survayNo: any ) => {
    this.modalService.open( id )
    this.transferModal = true
    this.survayNo = survayNo
  }
  change = async ( id: string, survayNo: any ) => {
    this.modalService.open( id )
    this.changeModal = true
    this.survayNo = survayNo
  }
  closeModal( id: string ) {
    this.modalService.close( id )
    this.transferModal = false
    this.changeModal = false
  }
  paytax = async ( survayNo: any, area: any ) => {
    const tax_price = await this.lba.tax_price().call( { from: this.account } )
    console.log( 'Log: UserComponent -> paytax -> tax_price', tax_price )
    const ethAmount = ( area * tax_price )
    console.log( 'Log: UserComponent -> paytax -> ethAmount', ethAmount )
    const regRespone = await this.lba.PayTax( parseInt( survayNo ) ).send( {
      from: this.account,
      gas: 5000000,
      value: ethAmount
    } )

    console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
    if ( regRespone.status ) {
      this.modalService.close( 'custom-modal' )
      alert( 'Payment Id ' + regRespone.events.TaxPayment.returnValues.transcationId + '  is Generated Successfully' )
    }
  }

  onSubmit = async ( form: NgForm ) => {
    let regRespone
    try {
      if ( Object.values( form.value ).length == 2 ) {
        const data: any = form.value
        console.log( 'Log: UserComponent -> onSubmit -> data', data )
        regRespone = await this.lba.TransferLand(
          parseInt( this.survayNo ),
          parseInt( data.area ),
          data.receiver,
        ).send( { from: this.account, gas: 5000000 } )
        console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
        if ( regRespone.status ) {
          this.modalService.close( 'custom-modal' )
          alert( 'Land ' + regRespone.events.LandTranscation.returnValues.surveyNo + '  is Transferd Successfully' )
        }
      }
      if ( Object.values( form.value ).length == 1 ) {
        const data: any = form.value
        regRespone = await this.lba.ChangeLandProperty(
          parseInt( this.survayNo ),
          parseInt( data.type )
        ).send( { from: this.account, gas: 5000000 } )
        console.log( 'Log: AdminComponent -> onSubmit -> regRespone', regRespone )
        if ( regRespone.status ) {
          this.modalService.close( 'custom-modal' )
          alert( 'Change Land id ' + regRespone.events.ChangeLand.returnValues.changeLandId + '  is Generated Successfully' )
        }
      }
      if ( regRespone.status ) {
        this.userLandList = []
        this.dataSource = null
        this.onLoad()
      }
    } catch ( error ) {
      console.log( 'Log: UserComponent -> onSubmit -> error', error )

    }
  }


}
