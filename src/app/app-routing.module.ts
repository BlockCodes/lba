import { ViewDetailsComponent } from './Components/view-details/view-details.component'
import { UserGuard } from './Guards/User/user.guard'
import { OfficerGuard } from './Guards/Officer/officer.guard'
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { HomeComponent } from './Components/home/home.component'
import { AboutComponent } from './Components/about/about.component'
import { FaqComponent } from './Components/faq/faq.component'
import { AdminComponent } from './Components/admin/admin.component'
import { OfficerRouterComponent } from './Components/officer-router/officer-router.component'
import { OfficerComponent } from './Components/officer-router/officer/officer.component'
import { LandRegComponent } from './Components/officer-router/land-reg/land-reg.component'
import { ApproveComponent } from './Components/officer-router/approve/approve.component'
import { UserRegComponent } from './Components/user-reg/user-reg.component'
import { UserRouterComponent } from './Components/user-router/user-router.component'
import { UserComponent } from './Components/user-router/user/user.component'
import { UserApproveComponent } from './Components/user-router/user-approve/user-approve.component'
import { AdminGuard } from './Guards/Admin/admin.guard'


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'user-reg',
    component: UserRegComponent
  },
  {
    path: 'view',
    component: ViewDetailsComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [ AdminGuard ]
  },
  {
    path: 'officer',
    component: OfficerRouterComponent,
    canActivate: [ OfficerGuard ],
    children: [
      {
        path: '',
        component: OfficerComponent
      },
      {
        path: 'officerReg',
        component: LandRegComponent
      },
      {
        path: 'officerApprove',
        component: ApproveComponent
      }
    ]
  },
  {
    path: 'user',
    component: UserRouterComponent,
    canActivate: [ UserGuard ],
    children: [
      {
        path: '',
        component: UserComponent
      },
      {
        path: 'user-approve',
        component: UserApproveComponent
      },
    ]
  }
]

@NgModule( {
  imports: [ RouterModule.forRoot( routes ) ],
  exports: [ RouterModule ]
} )
export class AppRoutingModule { }
